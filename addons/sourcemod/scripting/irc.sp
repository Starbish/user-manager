/*
 * ============================================================================
 *
 *  Internet Relay Chat (IRC) (C)2015-2016 Starbish. All rights reserved.
 *
 * ============================================================================
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ============================================================================
 */

#define _INTERNET_RLEAY_CHAT_INCLUDED_
#pragma semicolon 1

#include <sourcemod>
#include <socket>
#include <stlib>

#define INTERNAL_MESSAGE_ADVERTISE "server-advertise"

#define MAX_SERVERNAME_SIZE 100

#define PLUGIN_MAIN_VERSION "In Progress"

public Plugin myinfo = 
{
    name 		=	"IRC for Server",
    author		=	PLUGIN_WRITER,
    description	=	"Internet based Relay Chat System",
    version		=	PLUGIN_MAIN_VERSION,
    url			=	PLUGIN_WRITER_HOMEPAGE
};

Handle g_hIRCSocket;

ConVar g_ConVarIRCMasterServer;
ConVar g_ConVarIRCPassword;
ConVar g_ConVarIRCMainServerIP;
ConVar g_ConVarIRCMainServerPort;
ConVar g_ConVarIRCServerName;
ConVar g_ConVarIRCReconnectDelay;

//Handle g_hGlobalForward_IRC_OnMasterServerReady;

ArrayList g_arrIRCServerName;
ArrayList g_arrIRCServerList;
ArrayList g_arrIRCBanlist;
ArrayList g_arrIRCBanlistPort;

public APLRes AskPluginLoad2(Handle hMyself, bool bLateLoad, char[] cError, int iMaxLength){

	// Natives
	CreateNative("IRC_InternalMessage", Native_IRC_InternalMessage);
	CreateNative("IRC_BroadcastMessage", Native_IRC_BroadcastMessage);

}

public void OnPluginStart(){

	// ConVar
	g_ConVarIRCMasterServer		=	CreateConVar("irc_masterserver", "1", "Master Server = 1, Sub Server = 0");
	g_ConVarIRCPassword			=	CreateConVar("irc_password", "should be changed.", "IRC Master Server Password");
	g_ConVarIRCMainServerIP		=	CreateConVar("irc_masterserverip", "", "IRC Master Server IP");
	g_ConVarIRCMainServerPort	=	CreateConVar("irc_masterserverport", "25000", "IRC Master Server Port");
	g_ConVarIRCServerName		=	CreateConVar("irc_servername", "Server Name to be broadcasted", "Server Name to be broadcasted");
	g_ConVarIRCReconnectDelay	=	CreateNative("irc_reconnectdelay", "10.0", "Reconnect delay when server is not responding.");

	// forward
//	g_hGlobalForward_IRC_OnMasterServerReady = CreateGlobalForward("IRC_OnMasterServerReady", ET_Ignore);

	// Connect
	IRC_Internal_ReadySocket();
}

public void OnMapStart(){

	char cServerName[MAX_SERVERNAME_SIZE];
	char cMapName[PLATFORM_MAX_PATH];

	g_ConVarIRCMainServerName.GetString(cServerName, sizeof(cServerName));
	GetCurrentMap(cMapName, sizeof(cMapName));

	IRC_Internal_BroadcastMessage("[%s] 서버의 맵이 %s 로 변경되었습니다.", cServerName, cMapName);
	IRC_Internal_InternalMessage(INTERNAL_MESSAGE_ADVERTISE);

}

void IRC_Internal_ReadySocket(){

	if(SocketIsConnected(g_hIRCSocket)){

		PrintToServer("[IRC] Server's already connected.");
		return;
	}

	int iPort = g_ConVarIRCMainServerPort.IntValue;
	char cMasterServerIP[MAX_IPV6_SIZE];
	g_ConVarIRCMainServerIP.GetString(cMasterServerIP, sizeof(cMasterServerIP));

	g_hIRCSocket = SocketCreate(SOCKET_TCP, IRC_OnSocketError);

	// if it's client-server
	if(g_ConVarIRCMasterServer.IntValue == 0){

		PrintToServer("[IRC] Connecting to Master Server......");
		SocketConnect(g_hIRCSocket, IRC_OnSocketConnected, IRC_OnSocketReceived, IRC_OnSocketDisconnected, cMasterServerIP, iPort);

	} else {

		PrintToServer("[IRC] Initializing Master Server......");
		SocketSetOption(g_hIRCSocket, SocketReuseAddr, view_as<int>(true));
		SocketBind(g_hIRCSocket, cMasterServerIP, iPort);	
		SocketListen(g_hIRCSocket, IRC_OnSocketIncoming);

		if(g_arrIRCServerName != null){

			delete g_arrIRCServerName;
			g_arrIRCServerName = new ArrayList( ByteCountToCells(MAX_SERVERNAME_SIZE) );
		}

		if(g_arrIRCServerList != null){

			delete g_arrIRCServerList;
			g_arrIRCServerList = new ArrayList( ByteCountToCells(MAX_IPV6_SIZE) );
		}

		if(g_arrIRCBanlist != null){

			delete g_arrIRCBanlist;			
			g_arrIRCBanlist = new ArrayList( ByteCountToCells(MAX_IPV6_SIZE) );
		}

		if(g_arrIRCBanlistPort != null){

			delete g_arrIRCBanlistPort;
			g_arrIRCBanlistPort = new ArrayList();
		}
	}
}

// IRC_OnSocketError
public void IRC_OnSocketError(Handle hSocket, const int iErrorType, const int numError, any arg){

	PrintToServer("[Socket Error] ErrorType : %d || numError : %d\nRetry after %0.f Seconds....", iErrorType, numError, g_ConVarIRCReconnectDelay.FloatValue);

	CreateTimer(g_ConVarIRCReconnectDelay.FloatValue, Timer_ReconnectIRC, _, TIMER_FLAG_NO_MAPCHANGE);

}

public Action Timer_ReconnectIRC(Handle hTimer){

	IRC_Internal_ReadySocket();

}

// callback for both server and client

// for server only

// for client only
public void IRC_OnSocketConnected(Handle hSocket, any arg){

	char cServerName[MAX_SERVERNAME_SIZE];
	g_ConVarIRCMainServerName.GetString(cServerName, sizeof(cServerName));

	IRC_Internal_BroadcastMessage("[%s] 서버가 연결되었습니다.", cServerName);
	IRC_Internal_InternalMessage(INTERNAL_MESSAGE_ADVERTISE);

	PrintToServer("[Socket] Successfully connected to Master Server.");

}

public void IRC_OnSocketReceived(Handle hSocket, const char[] cPacket, const int iPacketSize, any arg){

	// don't suspect until its packet is from master server.

	if(Found_Password == 0)
	{
		PrintToChatAll(Message);
	}
	else
	{
		PrintToServer("[CBOS] Connection has been rejected (Wrong password)");
		Socket_Clear(socket);
	}
}
