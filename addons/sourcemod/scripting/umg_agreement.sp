/*
 * ============================================================================
 *
 *  User Manager | Agreement (C)2015-2016 Starbish. All rights reserved.
 *
 * ============================================================================
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ============================================================================
 */

#define _UMG_AGREEMENT_PLUGIN_
#pragma semicolon 1

#include <sourcemod>
#include <stlib>
#include <umg/agreement/const>
#include <umg>

#define PLUGIN_MAIN_VERSION "In Progress"

public Plugin myinfo = 
{
    name 		=	"User-Manager | Agreement",
    author		=	PLUGIN_WRITER,
    description	=	"Agreement Module for User-Manager",
    version		=	PLUGIN_MAIN_VERSION,
    url			=	PLUGIN_WRITER_HOMEPAGE
};

bool g_bDataLoaded[MAXPLAYERS+1];
bool g_bAgreed[MAXPLAYERS+1];

public void OnPluginStart(){

	HookEvent("player_activate", EventHook_PlayerActivate);
	HookEvent("player_team", EventHook_PlayerTeam);
	HookEvent("player_spawn", EventHook_PlayerSpawn);

	RegConsoleCmd("jointeam", ConsoleHook_JoinTeam);

}

public Action ConsoleHook_JoinTeam(int client, int iArgs){

	char cArg1[5];
	GetCmdArg(1, cArg1, sizeof(cArg1));

	if(g_bDataLoaded[client] && !g_bAgreed[client]){

		UMG_AgreementConfirmation(client);
		UMG_PrintMessage(client, "You should agree our rules first to play this game.");

		return Plugin_Handled;
	}

	return Plugin_Continue;
}

public Action EventHook_PlayerActivate(Handle hEvent, const char[] cEventName, bool bNotBroadcast){

	int client = GetClientOfUserId(GetEventInt(hEvent, "userid"));

	if(g_bDataLoaded[client] && !g_bAgreed[client])
		UMG_AgreementConfirmation(client);

	UMG_PrintAlert("player_activate");
}

public Action EventHook_PlayerTeam(Handle hEvent, const char[] cEventName, bool bNotBroadcast){

	int client = GetClientOfUserId(GetEventInt(hEvent, "userid"));

	if(g_bDataLoaded[client] && !g_bAgreed[client]){

		UMG_AgreementConfirmation(client);
		ChangeClientTeam(client, 0);
	}
}

public Action EventHook_PlayerSpawn(Handle hEvent, const char[] cEventName, bool bNotBroadcast){

	int client = GetClientOfUserId(GetEventInt(hEvent, "userid"));

	if(g_bDataLoaded[client] && !g_bAgreed[client] && !IsFakeClient(client)){

		UMG_AgreementConfirmation(client);
		ChangeClientTeam(client, 0);
	}
}

public void UMG_OnDatabaseReady(Database dbIndex){

	UMG_PresetDatabase(dbIndex);

}

public void UMG_OnUserDataRetrieved(int client, ArrayList arrTableList, DBResultSet[] dbrUserData){

	int iTableIdx;
	int iFieldIdx;

	for(int i = 0; i < arrTableList.Length; i++){

		// UserInformation
		if( (iTableIdx = arrTableList.FindString("userinformation")) != -1 ){

			g_bDataLoaded[client] = true;

			if(dbrUserData[iTableIdx].FieldNameToNum(UMG_AGREEMENT_FIELDNAME, iFieldIdx))
				g_bAgreed[client] = view_as<bool>( dbrUserData[iTableIdx].FetchInt(iFieldIdx) );

			if(!g_bAgreed[client])
				UMG_AgreementConfirmation(client);

			// if plugin was late-loaded and disagreed user was playing....
			if(IsClientInGame(client)){

				if(GetClientTeam(client) > 1 && !g_bAgreed[client]){

					UMG_AgreementConfirmation(client);

					// then move him back to spectator.
					ChangeClientTeam(client, 1);
				}
			}
		}
	}
}

void UMG_AgreementConfirmation(int client){

	Menu mHandle = new Menu(AgreementConfirmation_MenuHandler);
	mHandle.SetTitle("||||----==========| 동의서 |==========----||||");
	mHandle.AddItem("", "본 서버는 유저분들의 아이피와 닉네임을 수집합니다.",ITEMDRAW_DISABLED);
	mHandle.AddItem("", "이에 동의하시고 플레이 하시려면 \"예\"를 눌러주시고,", ITEMDRAW_DISABLED);
	mHandle.AddItem("", "동의하지 않으시고 나가시려면 \"아니요\"를 눌러주세요.", ITEMDRAW_DISABLED);
	mHandle.AddItem("1", "예");
	mHandle.AddItem("0", "아니요");

	mHandle.Display(client, 100);

}

public int AgreementConfirmation_MenuHandler(Menu mHandle, MenuAction actIndex, int iParam1, int iParam2){

	if(actIndex == MenuAction_Select){

		char cInfoNumber[2];
		if(mHandle.GetItem(iParam2, cInfoNumber, sizeof(cInfoNumber))){

			int iInfo = StringToInt(cInfoNumber);

			if(iInfo == 1){

				g_bAgreed[iParam1] = true;

				char cAuthId[MAX_AUTHID_STEAMID64];
				char cQuery[512];

				GetClientAuthId(iParam1, AuthId_SteamID64, cAuthId, sizeof(cAuthId));
				Format(cQuery, sizeof(cQuery), "UPDATE userinformation SET %s = 1 WHERE SteamID64 = '%s';", UMG_AGREEMENT_FIELDNAME, cAuthId);

				UMG_GetMainDatabase().Query(SQL_NoCallback, cQuery);

			}

			else
				UMG_AgreementReconfirmation(iParam1);

		}

		delete mHandle;
	}
}

void UMG_AgreementReconfirmation(int client){

	Menu mHandle = new Menu(AgreementReconfirmation_MenuHandler);
	mHandle.SetTitle("[ 유저 동의 재확인 ]");
	mHandle.AddItem("", "정말로 나가시겠습니까?", ITEMDRAW_DISABLED);
	mHandle.AddItem("1", "예");
	mHandle.AddItem("0", "아니요");

	mHandle.Display(client, 100);

}

public int AgreementReconfirmation_MenuHandler(Menu mHandle, MenuAction actIndex, int iParam1, int iParam2){

	if(actIndex == MenuAction_Select){

		char cInfoNumber[2];
		if(mHandle.GetItem(iParam2, cInfoNumber, sizeof(cInfoNumber))){

			int iInfo = StringToInt(cInfoNumber);

			if(iInfo == 1)
				KickClient(iParam1);
			else
				UMG_AgreementConfirmation(iParam1);

			delete mHandle;
		}
	}
}