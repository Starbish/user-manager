/*
 * ============================================================================
 *
 *  User Manager | VIP (C)2015-2016 Starbish. All rights reserved.
 *
 * ============================================================================
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ============================================================================
 */

#define _UMG_VIP_PLUGIN_
#pragma semicolon 1

#include <sourcemod>
#include <stlib>
#include <umg/const>
#include <umg/vip/const>

#define PLUGIN_MAIN_VERSION "In Progress"

public Plugin myinfo = 
{
    name 		=	"User-Manager | VIP",
    author		=	PLUGIN_WRITER,
    description	=	"VIP Module for User-Manager",
    version		=	PLUGIN_MAIN_VERSION,
    url			=	PLUGIN_WRITER_HOMEPAGE
};

// core's
#include <umg/pfunction>
#include <umg/api>

int g_iVIPLevel[MAXPLAYERS+1];

Handle g_hGlobalForward_UMG_OnVIPLevelReady;
Handle g_hGlobalForward_UMG_OnVIPLevelChanged;

// vip
#include <umg/vip/native>

public APLRes AskPluginLoad2(Handle hMyself, bool bLateLoad, char[] cError, int iMaxLength){

	UMG_RegPluginLibrary("VIP");

	// Natives
	CreateNative("UMG_GetUserVIPLevel", Native_UMG_GetUserVIPLevel);
	CreateNative("UMG_SetUserVIPLevel", Native_UMG_SetUserVIPLevel);

}

public void OnPluginStart(){

	// forward
	g_hGlobalForward_UMG_OnVIPLevelReady = CreateGlobalForward("UMG_OnVIPLevelReady", ET_Ignore, Param_Cell, Param_Cell);
	g_hGlobalForward_UMG_OnVIPLevelChanged = CreateGlobalForward("UMG_OnVIPLevelChanged", ET_Ignore, Param_Cell, Param_Cell, Param_Cell);

}

// User-Manager Forward
public void UMG_OnDatabaseReady(Database dbIndex){

	UMG_PresetDatabase(dbIndex);

}

public void UMG_OnUserDataRetrieved(int client, ArrayList arrTableList, DBResultSet[] dbrUserData){

	int iTableIdx;
	int iFieldIdx;

	for(int i = 0; i < arrTableList.Length; i++){

		// UserInformation
		if( (iTableIdx = arrTableList.FindString("userinformation")) != -1 ){

			if(dbrUserData[iTableIdx].FieldNameToNum(UMG_VIP_FIELDNAME, iFieldIdx)){

				g_iVIPLevel[client] = dbrUserData[iTableIdx].FetchInt( iFieldIdx );

				if(g_iVIPLevel[client] != UMG_VIPLEVEL_NONE){

					Call_StartForward(g_hGlobalForward_UMG_OnVIPLevelReady);

					Call_PushCell(client);
					Call_PushCell(g_iVIPLevel[client]);

					Call_Finish();

				}
			}
		}

		else UMG_PrintAlert("No matches (VIP)");
	}
}