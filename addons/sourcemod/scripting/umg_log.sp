/*
 * ============================================================================
 *
 *  User Manager - Log (C)2015-2016 Starbish. All rights reserved.
 *
 * ============================================================================
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ============================================================================
 */

#pragma semicolon 1

#include <sourcemod>
#include <stlib>
#include <umg/const>
#include <umg/log/const>

#define _UMG_LOG_PLUGIN_
#define PLUGIN_MAIN_VERSION "In Progress"

public Plugin myinfo = 
{
    name 		=	"User-Manager | Log",
    author		=	PLUGIN_WRITER,
    description	=	"Log Module for User-Manager",
    version		=	PLUGIN_MAIN_VERSION,
    url			=	PLUGIN_WRITER_HOMEPAGE
};

// core's
#include <umg/pfunction>
#include <umg/api>

Database g_dbLog;

// ConVar
ConVar g_ConVarLogDatabaseConfigName;
ConVar g_ConVarLogDatabaseConnectRetryDelay;

// forwards
//Handle g_hGlobalForward_UMG_OnLogRecorded;

int g_iMonthDividerIdx = -1;

// log
#include <umg/log/function>
#include <umg/log/native>
#include <umg/log/database>

public APLRes AskPluginLoad2(Handle hMyself, bool bLateLoad, char[] cError, int iMaxLength){

	UMG_RegPluginLibrary("Log");

	// Natives
	CreateNative("UMG_RecordLog", Native_UMG_RecordLog);

}

public void OnPluginStart(){

	// ConVar
	g_ConVarLogDatabaseConfigName = CreateConVar("umg_log_dbcfgname", "umg-log", "Config Name of UMG's Log Database Setting");
	g_ConVarLogDatabaseConnectRetryDelay = CreateConVar("umg_dbreconnectdelay", "5.0", "Delay of reconnect to log database (second)");

	// Forward
//	g_hGlobalForward_UMG_OnLogRecorded = CreateGlobalForward("UMG_OnLogRecorded", ET_Ignore, Param_String);

	// db connect
	char cDatabaseName[15];
	g_ConVarLogDatabaseConfigName.GetString(cDatabaseName, sizeof(cDatabaseName));

	Database.Connect(UMG_SQL_LogConnect, cDatabaseName);

}