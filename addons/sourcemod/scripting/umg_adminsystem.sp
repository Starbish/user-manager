/*
 * ============================================================================
 *
 *  User Manager | Admin-System (C)2015-2016 Starbish. All rights reserved.
 *
 * ============================================================================
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ============================================================================
 */

#define _UMG_ADMIN_SYSTEM_PLUGIN_
#pragma semicolon 1

#include <sourcemod>
#include <stlib>
#include <umg>
#include <umg/admin-system/const>

#define PLUGIN_MAIN_VERSION "In Progress"

public Plugin myinfo = 
{
    name 		=	"User-Manager | Admin-System",
    author		=	PLUGIN_WRITER,
    description	=	"Admin System for User-Manager",
    version		=	PLUGIN_MAIN_VERSION,
    url			=	PLUGIN_WRITER_HOMEPAGE
};

char g_cAdminName[MAXPLAYERS+1][MAX_ADMINLEVEL_NAME];
bool g_bAdminLevelAuthorized[MAXPLAYERS+1];

ArrayList g_arrAdminCommandList;

public APLRes AskPluginLoad2(Handle hMyself, bool bLateLoad, char[] cError, int iMaxLength){

	UMG_RegPluginLibrary("Admin-System");

	// Natives
	CreateNative("UMG_GetUserAdminLevel", Native_UMG_GetUserAdminLevel);
	CreateNative("UMG_IsUserAdminAuthChecked", Native_UMG_IsUserAdminAuthChecked);
}

public void OnPluginStart(){

	// Dynamic Array
	g_arrAdminCommandList = new ArrayList( ByteCountToCells(MAX_ADMINLEVEL_NAME) );

	// Basic Features for Admin-Command, for korean.
	UMG_RegisterAdminCommand("맵변경", UMG_AdminCommand_ChangeLevel, ADMINAUTH_CHANGEMAP);
	UMG_RegisterAdminCommand("강제맵변경", UMG_AdminCommand_ForceChangeLevel, ADMINAUTH_FORCECHANGEMAP);
	UMG_RegisterAdminCommand("죽이기", UMG_AdminCommand_Slay, ADMINAUTH_SLAY);
	UMG_RegisterAdminCommand("추방", UMG_AdminCommand_Kick, ADMINAUTH_KICK);
	UMG_RegisterAdminCommand("임시정지", UMG_AdminCommand_TempBan, ADMINAUTH_TEMPBAN);
	UMG_RegisterAdminCommand("정지", UMG_AdminCommand_Ban, ADMINAUTH_BAN);
	UMG_RegisterAdminCommand("영구정지", UMG_AdminCommand_PermanentBan, ADMINAUTH_PERMANENTBAN);
	UMG_RegisterAdminCommand("영구정지취소", UMG_AdminCommand_PermanentUnban, ADMINAUTH_PERMANENTUNBAN);
	UMG_RegisterAdminCommand("정지취소", UMG_AdminCommand_Unban, ADMINAUTH_UNBAN);
	UMG_

}

public void OnAllPluginsLoaded(){

	ServerCommand("sm plugins unload adminhelp");
}

stock UMG_RegisterAdminType(const char[] cAdminName, int iAdminAuthority){


}

stock UMG_RegisterAdminCommand(const char[] cCommandName, ConCmd cmdCallback, int iAuthority){


}

// User-Manager Forward
public void UMG_OnDatabaseReady(Database dbIndex){

	UMG_PresetDatabase(dbIndex);

}

public void UMG_OnUserDataRetrieved(int client, ArrayList arrTableList, DBResultSet[] dbrUserData){

	int iTableIdx;
	int iFieldIdx;

	for(int i = 0; i < arrTableList.Length; i++){

		// UserInformation
		if( (iTableIdx = arrTableList.FindString("userinformation")) ){

			if(dbrUserData[iTableIdx].FieldNameToNum(UMG_ADMINLEVEL_FIENDNAME, iFieldIdx)){

				dbrUserData[iTableIdx].FetchString(iFieldIdx, g_cAdminName[client], sizeof(g_cAdminName[]) );
			}
		}
	}
}