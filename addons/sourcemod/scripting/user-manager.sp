/*
 * ============================================================================
 *
 *  User Manager - Core (C)2015-2016 Starbish. All rights reserved.
 *
 * ============================================================================
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ============================================================================
 */

#define _UMG_CORE_PLUGIN_
#pragma semicolon 1

#include <sourcemod>
#include <stlib>
#include <umg/const>

#define PLUGIN_MAIN_VERSION "In Progress"

public Plugin myinfo = 
{
    name 		=	"User-Manager",
    author		=	PLUGIN_WRITER,
    description	=	"Manage user's basic data",
    version		=	PLUGIN_MAIN_VERSION,
    url			=	PLUGIN_WRITER_HOMEPAGE
};

// Main Database Index
Database g_dbIndex;

// ConVars
ConVar g_ConVarServerMasterAuthId;
ConVar g_ConVarDatabaseConfigName;
ConVar g_ConVarDatabaseConnectRetryDelay;

// Forwards
Handle g_hGlobalForward_UMG_OnDatabaseReady;
Handle g_hGlobalForward_UMG_OnUserDataRetrieved;

int g_iUserDataLoadStatus[MAXPLAYERS+1];

#include <umg/pfunction>
#include <umg/internal_function>
#include <umg/database>
#include <umg/native>
#include <umg/api>

public APLRes AskPluginLoad2(Handle hMyself, bool bLateLoad, char[] cError, int iMaxLength){

	UMG_RegPluginLibrary("Core");

	// Natives
	CreateNative("UMG_GetMainDatabase", Native_UMG_GetMainDatabase);
	CreateNative("UMG_IsDatabaseAvailable", Native_UMG_IsDatabaseAvailable);
	CreateNative("UMG_RefreshUserData", Native_UMG_RefreshUserData);
	CreateNative("UMG_RefreshAllUserData", Native_UMG_RefreshAllUserData);

	CreateNative("UMG_GetUserDataLoadStatus", Native_UMG_GetUserDataLoadStatus);
	CreateNative("UMG_IsUserDataLoaded", Native_UMG_IsUserDataLoaded);
	CreateNative("UMG_IsMasterAdmin", Native_UMG_IsMasterAdmin);

}

public void OnPluginStart(){

	// Version
	CreateConVar("umg_core_version", PLUGIN_MAIN_VERSION, "UMG Core Plugin's Version");

	// ConVars
	g_ConVarServerMasterAuthId = CreateConVar("umg_masterauthid", "NOT-SPECIFIED", "Server Master's Steam-AuthId.");
	g_ConVarDatabaseConfigName = CreateConVar("umg_dbcfgname", "umg", "Config Name of UMG's Database Setting");
	g_ConVarDatabaseConnectRetryDelay = CreateConVar("umg_dbreconnectdelay", "5.0", "Delay of reconnect to database (second)");

	// Forwards
	g_hGlobalForward_UMG_OnDatabaseReady = CreateGlobalForward("UMG_OnDatabaseReady", ET_Ignore, Param_Cell);
	g_hGlobalForward_UMG_OnUserDataRetrieved = CreateGlobalForward("UMG_OnUserDataRetrieved", ET_Ignore, Param_Cell, Param_Cell, Param_Array);

	// Events
	//HookEvent("player_connect", EventHook_Post_PlayerConnect);
	HookEvent("player_disconnect", EventHook_Post_PlayerDisconnect);

	char cDatabaseName[15];
	g_ConVarDatabaseConfigName.GetString(cDatabaseName, sizeof(cDatabaseName));

	// SQL Connection
	UMG_DBConnect();

}

// when user's steamauth is confirmed
public void OnClientAuthorized(int client, const char[] cAuthId){

	// must do insert process before receiving.
	UMG_Process_RefreshUserData(client);

}

public Action EventHook_Post_PlayerDisconnect(Handle hEvent, const char[] cEventName, bool bNotBroadcast){

	int client = GetClientOfUserId(GetEventInt(hEvent, "userid"));

	g_iUserDataLoadStatus[client] = INFORMATION_STATUS_NOT_LOADED;

}

// 디버그용
public void UMG_OnUserDataRetrieved(int client, ArrayList arrTableList, DBResultSet[] dbrUserData){

	UMG_PrintAlert("User : %d ", client);
}