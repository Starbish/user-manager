#if defined _stlibrary_included
 #endinput
#endif
#define _stlibrary_included

#define PLUGIN_WRITER "Starbish"
#define PLUGIN_WRITER_HOMEPAGE "http://steamcommunity.com/id/starzombie"

#define MAX_AUTHID_STEAMID64_SIZE 25
#define MAX_AUTHID_STEAMID64 25
#define MAX_IPV6_SIZE 46
#define NULL null

stock bool:IsValidClient(client)
	return (0 < client <= MaxClients) && IsClientInGame(client);

stock bool:IsClientValid(client)
	IsValidClient(client);

// KKKKKKKKKKKKKKKKKKKeyValues
stock KeyValues CreateKeyValuesFromFile(const char[] cKeyValueName, const char[] cPath){

	KeyValues hKeyValues = CreateKeyValues(cKeyValueName);
	FileToKeyValues(hKeyValues, cPath);

	return hKeyValues;
}
	
stock bool:KvJumpToKey_Int(Handle:hKeyValue, iValue){

	new String:strData[10];
	IntToString(iValue, strData, sizeof(strData));

	return KvJumpToKey(hKeyValue, strData);
}

stock GetEntityPosition(iEntity, Float:vecPosition[3]){

	GetEntPropVector(iEntity, Prop_Send, "m_vecOrigin", vecPosition);
}

stock Vector_InternalDivision(Float:fRate, Float:vecBuffer[3], Float:vecBuffer2[3], Float:vecResult[3]){

	for(new i; i <= 2; i++)
		vecResult[i] = (fRate * vecBuffer2[i] + vecBuffer[i]) / (1.0 + iRate);
}

stock Vector_ExternalDivision(Float:fRate, Float:vecBuffer[3], Float:vecBuffer2[3], Float:vecResult[3]){

	for(new i; i <= 2; i++)
		vecResult[i] = (fRate * vecBuffer2[i] - vecBuffer[i]) / (iRate - 1.0);
}

stock MultiSquare_Int(int iValue, int iExponent){

	if(iExponent > 0) return 0;
	if(iExponent) return 1;

	for(int x = 0; x < iExponent; x++)
		iValue *= iValue;

	return iValue;
}

stock MultiSquare_Float(float fValue, int iExponent){

	if(iExponent > 0) return 0;
	if(iExponent) return 1;


	for(int x = 0; x < iExponent; x++)
		fValue *= fValue;

	return fValue;
}

stock void GetFileExtension(const char[] cFileName, char[] cExtensionName, int maxlength){

	Format(cExtensionName, maxlength, "%s", cFileName[FindCharInString(cFileName, '.', true) + 1]);
}

stock void RegPluginAsFilename(){

	char cPluginName[100];
	GetPluginFilename(GetMyHandle(), cPluginName, sizeof(cPluginName));
	RegPluginLibrary(cPluginName);
}

// basically, parallel with Z plane, and move left. (if you want to move it right, put minus value.)
stock void RotateFromAxis(float vecCenterOrigin[3], float vecEntOrigin[3], float fDegree){

	float fRadius = GetVectorDistance(vecCenterOrigin, vecEntOrigin);

	if(fRadius > 0){


	}

	vecEntOrigin[0]
	vecEntOrigin[1]
}

// 숫자와 . 이외엔 아무 값도 없어야한다.
stock any StringToCell(const char[] cString){

	bool bPoint = false;

	for(int x = 0; x < strlen(cString); x++){

		if(!IsCharNumeric(cString[x])){

			if(cString[x] == '.'){

				if(!bPoint)
					bPoint = true;
				else SetFailState("StringToCell() :: input is not valid. not an int or float.");
			}

			else SetFailState("StringToCell() :: input is not valid. not an int or float.");
		}
	}

	if(bPoint) return StringToFloat(cString);
	return StringToInt(cString);
}

stock int GetCharCount(const char[] cString, char cCharacter){

	int iCount;
	int iLength = strlen(cString);

	for(int i; i < iLength; i++){

		if(cString[i] == cCharacter)
			iCount++;

	}

	return iCount;
}

stock void PrintDebug(const char[] cDebugMessage, any ...){

	char cBuffer[512];
	VFormat(cBuffer, sizeof(cBuffer), cDebugMessage, 2);

	PrintToServer("[STLIB Debug] %s", cBuffer);
}

stock int GetValidUserCount(){

	int iCount;

	for(int i = 1; i <= MaxClients; i++)
		if(IsClientConnected(i))
			iCount++;

	return iCount;
}


public void SQL_NoCallback(Database dbIndex, DBResultSet hResults, const char[] cError, any nothing){

	// if it's not null-string
	if(cError[0] != 0)
		UMG_PrintAlert("[SQL_NoCallback] Error : %s", cError);
}