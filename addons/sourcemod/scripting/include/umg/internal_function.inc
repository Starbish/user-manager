/*
 * ============================================================================
 *
 *  User Manager - [Header] Internal Function (C)2015-2016 Starbish. All rights reserved.
 *
 * ============================================================================
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ============================================================================
 */

#if defined _UMG_CORE_INTERNAL_FUNCTION_INCLUDED_
	#endinput
#endif
#define _UMG_CORE_INTERNAL_FUNCTION_INCLUDED_


void UMG_RefreshUserData(int client){

	if(IsClientConnected(client)){

		g_dbIndex.Query(SQL_RefreshUserData_Callback, "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_KEY = 'PRI' AND COLUMN_NAME = 'SteamID64';");

		g_iUserDataLoadStatus[client] = INFORMATION_STATUS_LOADING;

	}
}

void UMG_RefreshAllUserData(){

	// find database's user-sided table.
	g_dbIndex.Query(SQL_RefreshAllUserData_Callback, "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_KEY = 'PRI' AND COLUMN_NAME = 'SteamID64';");

	for(int client = 1; client <= MaxClients; client++)
		if(IsClientConnected(client))
			g_iUserDataLoadStatus[client] = INFORMATION_STATUS_LOADING;
}