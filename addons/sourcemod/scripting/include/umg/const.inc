/*
 * ============================================================================
 *
 *  User Manager - [Header] Constant (C)2015-2016 Starbish. All rights reserved.
 *
 * ============================================================================
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ============================================================================
 */

#if defined _UMG_CORE_CONSTANT_INCLUDED_
	#endinput
#endif
#define _UMG_CORE_CONSTANT_INCLUDED_

// User-Load Status
#define INFORMATION_STATUS_NOT_LOADED 0
#define INFORMATION_STATUS_LOADING 1
#define INFORMATION_STATUS_LOADED 2

const char g_cTableList[][] = {

	"UserInformation"

};

// its element must not have | on its end.
const char g_cFieldList[][] = {

	"SteamID64 bigint PRIMARY KEY",
//	"idxHistory bigint PRIMARY KEY AUTO_INCREMENT | IP char(46) | Count int default 0",
//	"idxHistory bigint PRIMARY KEY AUTO_INCREMENT | Nickname char(32) | Count int default 0"

};

#define MAX_TABLENAME_SIZE sizeof(g_cTableList[]) + 50
#define MAX_FIELDLIST_SIZE sizeof(g_cFieldList[]) + 50
#define MAX_FIELD_DESCRIPTION_SIZE 100