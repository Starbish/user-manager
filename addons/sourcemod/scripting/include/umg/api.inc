/*
 * ============================================================================
 *
 *  User Manager - [Header] API (C)2015-2016 Starbish. All rights reserved.
 *
 * ============================================================================
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ============================================================================
 */

#if defined _UMG_CORE_API_INCLUDED_
	#endinput
#endif
#define _UMG_CORE_API_INCLUDED_

/**
 * User-Manager 의 메인 	데이터베이스가 준비될 때 호출된다.
 * 
 * @param dbIndex		데이터베이스 핸들
 */
forward void UMG_OnDatabaseReady(Database dbIndex);

/**
 * 메인 플러그인에서 유저의 정보를 불러왔을 때 호출된다.
 * 
 * @param client			클라이언트 인덱스
 * @param arrTableList		테이블 목록 동적배열
 * @param dbrUserData		테이블 목록에 대응하는 유저의 데이터
 */
forward void UMG_OnUserDataRetrieved(int client, ArrayList arrTableList, DBResultSet[] dbrUserData);

/**
 * User-Manager 의 데이터베이스 핸들 인덱스를 가져온다.
 *
 * @noparam
 * @return				메인 플러그인의 Database 메소드맵 핸들
 * @noerror				
 */
native Database UMG_GetMainDatabase();

/**
 * User-Manager 의 데이터베이스 사용 여부를 얻는다.
 *
 * @noparam
 * @return			데이터베이스가 사용 가능하면 true, 그렇지 않으면 false.
 * @noerror				
 */
native bool UMG_IsDatabaseAvailable();

/**
 * client 의 데이터를 갱신 혹은 얻어온다.
 *
 * @param client	클라이언트 인덱스
 * @noreturn
 * @noerror				
 */
native void UMG_RefreshUserData(int client);

/**
 * 서버에 들어온 전원의 데이터를 갱신 혹은 얻어온다.
 *
 * @noparam
 * @noreturn
 * @noerror				
 */
native void UMG_RefreshAllUserData();

/**
 * client 의 데이터 로드 상태를 얻는다.
 *
 * @param client	클라이언트 인덱스
 * @return			로드 상태 (umg/const 에서 그 값들을 확인할 수 있다.)
 * @noerror				
 */
native int UMG_GetUserDataLoadStatus(int client);

/**
 * client 의 데이터 로드 여부를 얻는다.
 *
 * @param client	클라이언트 인덱스
 * @return			클라이언트의 데이터가 불러져있으면 true, 그렇지 않으면 false.
 * @noerror				
 */
native bool UMG_IsUserDataLoaded(int client);

/**
 * client 가 최고 관리자인지 확인한다.
 *
 * @param client	클라이언트 인덱스
 * @return			최고 관리자면 true, 그렇지 않으면 false.
 * @noerror				
 */
native bool UMG_IsMasterAdmin(int client);
