/*
 * ============================================================================
 *
 *  User Manager - [Header] Native (C)2015-2016 Starbish. All rights reserved.
 *
 * ============================================================================
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ============================================================================
 */

#if defined _UMG_CORE_NATIVE_INCLUDED_
	#endinput
#endif
#define _UMG_CORE_NATIVE_INCLUDED_

public int Native_UMG_GetMainDatabase(Handle hPlugin, int numParams){

	return view_as<int>(g_dbIndex);

}

public int Native_UMG_IsDatabaseAvailable(Handle hPlugin, int numParams){

	return view_as<int>( (g_dbIndex != null ? true ; false) );

}

public int Native_UMG_RefreshUserData(Handle hPlugin, int numParams){

	if(g_dbIndex != null)
		UMG_RefreshUserData(GetNativeCell(1));

	else
		UMG_PrintAlert("[UMG_RefreshUserData] Database isn't ready yet.");

}

public int Native_UMG_RefreshAllUserData(Handle hPlugin, int numParams){

	if(g_dbIndex != null)
		UMG_RefreshAllUserData();

	else
		UMG_PrintAlert("[UMG_RefreshAllUSerData] Database isn't ready yet.");
		
}

public int Native_UMG_GetUserDataLoadStatus(Handle hPlugin, int numParams){

	return g_iUserDataLoadStatus[GetNativeCell(1)];
		
}

public int Native_UMG_IsUserDataLoaded(Handle hPlugin, int numParams){

	return view_as<int>( g_iUserDataLoadStatus[GetNativeCell(1)] == INFORMATION_STATUS_LOADED );
		
}

public int Native_UMG_IsMasterAdmin(Handle hPlugin, int numParams){

	int client = GetNativeCell(1);
	char cSteamID[MAX_AUTHID_STEAMID64_SIZE];
	char cMasterAuthID[MAX_AUTHID_STEAMID64_SIZE];

	g_ConVarServerMasterAuthId.GetString(cMasterAuthID, sizeof(cMasterAuthID));

	for(int i = 0; i < AuthIdType; i++){

		GetClientAuthId(client, i, cSteamID, sizeof(cSteamID));
		
		if(StrEqual(cSteamID, cMasterAuthID))
			return view_as<int>(true);

	}
	
	return view_as<int>(false);
}

