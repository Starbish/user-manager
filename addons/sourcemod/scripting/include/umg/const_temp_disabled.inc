/*
 * ============================================================================
 *
 *  User Manager - Constants (C)2015-2016 Starbish. All rights reserved.
 *
 * ============================================================================
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ============================================================================
 */

#define MAX_UMG_HANDLER_NAME_SIZE 60
#define MAX_UMG_INFO_NAME_SIZE 40
#define MAX_UMG_INFO_SIZE 512

#define UMG_INFOTYPE_UNKNOWN 0
#define UMG_INFOTYPE_INT ( 1 << 0 )
#define UMG_INFOTYPE_FLOAT ( 1 << 1 )
#define UMG_INFOTYPE_STRING ( 1 << 2 )
#define UMG_INFOTYPE_ARRAY ( 1 << 3 )