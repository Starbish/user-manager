/*
 * ============================================================================
 *
 *  User Manager - [Header] Database (C)2015-2016 Starbish. All rights reserved.
 *
 * ============================================================================
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ============================================================================
 */

#if defined _UMG_CORE_DATABASE_INCLUDED_
	#endinput
#endif
#define _UMG_CORE_DATABASE_INCLUDED_

public void UMG_SQL_Connect(Database dbIndex, const char[] cError, any iData){

	// connected.
	if(dbIndex != null){

		UMG_PrintAlert("Successfully connected to database.");
		g_dbIndex = dbIndex;

		// create default tables.
		Transaction txnHandle = new Transaction();

		int iPieceCount;

		char cQuery[512];
		char[][] cFields = new char[iPieceCount][MAX_FIELD_DESCRIPTION_SIZE];

		for(int i = 0; i < sizeof(g_cTableList); i++){

			iPieceCount = GetCharCount(g_cFieldList[i], "|") + 1;

			Format(cQuery, sizeof(cQuery), "CREATE TABLE IF NOT EXISTS %s(", g_cTableList[i]);
			ExplodeString(g_cFieldList[i], " | ", cFields, iPieceCount, sizeof(cFields[]));

			for(int x = 0; x < iPieceCount; x++){

				if(x != iPieceCount-1)
					Format(cQuery, sizeof(cQuery), "%s, ", cFields[x]);
				else
					Format(cQuery, sizeof(cQuery), "%s);", cFields[x]);
			}
		}

		dbIndex.Execute(txnHandle, SQL_TxnOnSuccess_CreateTable, _);

	} else {

		UMG_PrintAlert("Couldn't connect to database. Retry within 5 seconds.")

		CreateTimer(g_ConVarDatabaseConnectRetryDelay.FloatValue, Timer_DatabaseReconnect, _, TIMER_FLAG_NO_MAPCHANGE);
	
	}
}

public void SQL_TxnOnSuccess_CreateTable(Database dbIndex, any iData, int numQueries, DBResultSet[] hResults, any[] anyQueryData){

	// for late-load
	if(GetValidUserCount())
		UMG_RefreshAllUserData();

	// let moddules know database is ready to use.
	Call_StartForward(g_hGlobalForward_UMG_OnDatabaseReady);

	Call_PushCell(g_dbIndex);

	Call_Finish();

}

public Action Timer_DatabaseReconnect(Handle hTimer){

	UMG_PrintAlert("Retry connecting to database.......");

	char cDatabaseName[15];
	g_ConVarDatabaseConfigName.GetString(cDatabaseName, sizeof(cDatabaseName));

	Database.Connect(UMG_SQL_Connect, cDatabaseName);

}

public void SQL_RefreshUserData_Callback(Database dbIndex, DBResultSet hResults, const char[] cError, any nothing){

	int iRowCount = hResults.RowCount;

	if(iRowCount){

		ArrayList arrTableList = new ArrayList(ByteCountToCells(MAX_TABLENAME_SIZE));
		char[][] cTableList = new char[iRowCount][MAX_TABLENAME_SIZE];

		for(int i = 0; i < iRowCount; i++){

			hResults.FetchRow();
			hResults.FetchString(0, cTableList[i], sizeof(cTableList[]));
			arrTableList.PushString(cTableList[i]);

		}

		DataPack dpHandle;
		Transaction txnHandle = new Transaction();

		char cQuery[512];
		char cSteamID64[MAX_AUTHID_STEAMID64_SIZE];

		GetClientAuthId(client, AuthId_SteamID64, cSteamID64, sizeof(cSteamID64));

		for(int x = 0; x < iRowCount; x++){

			dpHandle = new DataPack();
			dpHandle.WriteString(cTableList[x]);

			Format(cQuery, sizeof(cQuery), "SELECT * FROM %s WHERE SteamID64 = '%s';", cTableList[x], cSteamID64);
			txnHandle.AddQuery(cQuery, dpHandle);

		}

		g_dbIndex.Execute(txnHandle, SQL_TxnOnSuccess_RefreshUserData, SQL_TxnOnFailure_RefreshUserData, arrTableList);

	} else {

		UMG_PrintAlert("Couldn't refresh user data. Error : %s", cError);

	}
}

public void SQL_TxnOnSuccess_RefreshUserData(Database dbIndex, any data, int numQueries, DBResultSet[] dbrQueryResult, any[] dpHandle){

	ArrayList arrTableList = view_as<ArrayList>( data );

	int iArrayIdx;
	char[][] cTableList = new char[arrTableList.Length][MAX_TABLENAME_SIZE];

	int iDBResultCount;
	DBResultSet[] dbrHandle = new DBResultSet[arrTableList.Length];

	for(int i = 0; i < numQueries; i++){

		dpHandle.ReadString(cTableList[iArrayIdx], sizeof(cTableList[]));
		iArrayIdx++;

		dbrHandle[iDBResultCount] = dbrQueryResult[i];
		iDBResultCount++;

		delete dpHandle[i];
	}

	Call_StartForward(g_hGlobalForward_UMG_OnUserDataRetrieved);

	Call_PushCell(client);
	Call_PushCell(arrTableList);
	Call_PushArray(dbrHandle);

	Call_Finish();

}

public void SQL_TxnOnFailure_RefreshUserData((Database dbIndex, any data, int numQueries, const char[] cError, int iIdxFail, any[] dpHandle){

	// for preventing memory-leak
	for(int i = 0; i < numQueries; i++)
		delete dpHandle[i];

	UMG_PrintAlert("[SQL_TxnOnFailure_RefreshUserData][Fail Index : %d] Error : %s", iIdxFail, cError);

}

////////////////////////////////// for all-user /////////////////////////////////////

public void SQL_RefreshAllUserData_Callback(Database dbIndex, DBResultSet hResults, const char[] cError, any nothing){

	int iRowCount = hResults.RowCount;

	if(iRowCount){

		ArrayList arrTableList = new ArrayList(ByteCountToCells(MAX_TABLENAME_SIZE));
		char[][] cTableList = new char[iRowCount][MAX_TABLENAME_SIZE];

		for(int i = 0; i < iRowCount; i++){

			hResults.FetchRow();
			hResults.FetchString(0, cTableList[i], sizeof(cTableList[]));
			arrTableList.PushString(cTableList[i]);

		}

		DataPack dpHandle;
		Transaction txnHandle = new Transaction();

		char cQuery[512];
		char cSteamID64[MAX_AUTHID_STEAMID64_SIZE];

		for(int i = 1; i <= MaxClients; i++){

			if(IsClientConnected(i)){

				GetClientAuthId(client, AuthId_SteamID64, cSteamID64, sizeof(cSteamID64));

				for(int x = 0; x < iRowCount; x++){

					dpHandle = new DataPack();

					dpHandle.WriteCell(i);
					dpHandle.WriteString(cTableList[x]);

					Format(cQuery, sizeof(cQuery), "SELECT * FROM %s WHERE SteamID64 = '%s';", cTableList[x], cSteamID64);
					txnHandle.AddQuery(cQuery, dpHandle);

				}
			}
		}

		g_dbIndex.Execute(txnHandle, SQL_TxnOnSuccess_RefreshAllUserData, SQL_TxnOnFailure_RefreshAllUserData, arrTableList);

	} else {

		UMG_PrintAlert("Couldn't refresh all of users data. Error : %s", cError);

	}
}

public void SQL_TxnOnSuccess_RefreshAllUserData(Database dbIndex, any data, int numQueries, DBResultSet[] dbrQueryResult, any[] dpHandle){

	ArrayList arrTableList = view_as<ArrayList>( data );

	int[] iUserIdx = new int[numQueries];
	int[] iArrayIdx = new int[MaxClients];
	char[][][] cTableList = new char[MaxClients][arrTableList.Length][MAX_TABLENAME_SIZE];

	int[] iDBResultCount = new int[MaxClients];
	DBResultSet[][] dbrHandle = new DBResultSet[MaxClients][arrTableList.Length];

	for(int i = 0; i < numQueries; i++){

		iUserIdx[i] = dpHandle[i].ReadCell();
		dpHandle[i].ReadString(cTableList[iUserIdx[i]][iArrayIdx[iUserIdx[i]]], sizeof(cTableList[][]));
		iArrayIdx[iUserIdx[i]]++;

		dbrHandle[iUserIdx[i]][iDBResultCount] = dbrQueryResult[i];
		iDBResultCount[iUserIdx[i]]++;

		// freed 
		delete dpHandle[i];

	}

	for(int client = 1; client <= MaxClients; client++){

		if(IsClientConnected(client)){

			g_iUserDataLoadStatus[client] = INFORMATION_STATUS_LOADED;

			Call_StartForward(g_hGlobalForward_UMG_OnUserDataRetrieved);

			Call_PushCell(client);
			Call_PushCell(arrTableList);
			Call_PushArray(dbrHandle[client]);

			Call_Finish();

		}
	}
}

public void SQL_TxnOnFailure_RefreshAllUserData((Database dbIndex, any data, int numQueries, const char[] cError, int iIdxFail, any[] dpHandle){

	// for preventing memory-leak
	for(int i = 0; i < numQueries; i++)
		delete dpHandle[i];

	UMG_PrintAlert("[SQL_TxnOnFailure_RefreshAllUserData][Fail Index : %d] Error : %s", iIdxFail, cError);

}