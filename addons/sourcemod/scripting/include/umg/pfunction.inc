/*
 * ============================================================================
 *
 *  User Manager - [Header] Platform Functions (C)2015-2016 Starbish. All rights reserved.
 *
 * ============================================================================
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ============================================================================
 */

#if defined _UMG_CORE_PLATFORM_FUNCTIONS_INCLUDED_
	#endinput
#endif
#define _UMG_CORE_PLATFORM_FUNCTIONS_INCLUDED_

/**
 * User-Manager 로서 플레이어에게 메시지를 보낸다.
 *
 * @param client		클라이언트 인덱스
 * @param cMessage		보낼 메시지.
 * @noreturn
 * @noerror
 */
stock void UMG_PrintMessage(int client, const char[] cMessage, any ...){

	char cBuffer[512];
	VFormat(cBuffer, sizeof(cBuffer), cMessage, 2);

	PrintToChat(client, cBuffer);
}

/**
 * User-Manager 로서 서버에 메시지를 보낸다.
 *
 * @param cDebugMessage		보낼 메시지.
 * @noreturn
 * @noerror
 */
stock void UMG_PrintAlert(const char[] cDebugMessage, any ...){

	char cBuffer[512];
	VFormat(cBuffer, sizeof(cBuffer), cDebugMessage, 2);

	PrintToServer("[User-Manager] %s", cBuffer);
}

/**
 * User-Manager 만의 특정 포맷으로, cPluginName 을 포함한 문자열로
 * 플러그인 라이브러리에 등록한다.
 *
 * @param cPluginName		등록할 역할 이름
 * @noreturn
 * @noerror
 */
stock void UMG_RegPluginLibrary(const char[] cPluginName){

	char cBuffer[300];
	Format(cBuffer, sizeof(cBuffer), "User-Manager :: %s", cPluginName);

	RegPluginLibrary(cBuffer);
}