/*
 * ============================================================================
 *
 *  User Manager - Credit (C)2015-2016 Starbish. All rights reserved.
 *
 * ============================================================================
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ============================================================================
 */

#define _UMG_CREDIT_PLUGIN_
#pragma semicolon 1

#include <sourcemod>
#include <stlib>
#include <umg/const>
#include <umg/credit/const>

#define PLUGIN_MAIN_VERSION "In Progress"

public Plugin myinfo = 
{
    name 		=	"User-Manager | Credit",
    author		=	PLUGIN_WRITER,
    description	=	"Credit Module for User-Manager",
    version		=	PLUGIN_MAIN_VERSION,
    url			=	PLUGIN_WRITER_HOMEPAGE
};

ArrayList g_arrCreditList;
ArrayList g_arrCreditDBFieldName;
ArrayList g_arrUserCredit;

#include <umg/pfunction>
#include <umg/api>

// forwards
Handle g_hGlobalForward_UMG_OnCreditRegistered;
Handle g_hGlboalForward_UMG_OnTransactionPerform;
Handle g_hGlobalForward_UMG_OnTransactionSucceeded;
Handle g_hGlobalForward_UMG_OnTransactionFailed;

// ConVar
ConVar g_ConVarCreditTableName;

// credit
#include <umg/credit/function>
#include <umg/credit/native>

public APLRes AskPluginLoad2(Handle hMyself, bool bLateLoad, char[] cError, int iMaxLength){

	UMG_RegPluginLibrary("Credit");

	// Natives
	CreateNative("UMG_RegisterCredit", Native_UMG_RegisterCredit);
	CreateNative("UMG_GetCredit", Native_UMG_GetCredit);
	CreateNative("UMG_SetCredit", Native_UMG_SetCredit);
	CreateNative("UMG_AddCredit", Native_UMG_AddCredit);
	CreateNative("UMG_TransactGoods", Native_UMG_TransactGoods);

}

public void OnPluginStart(){

	// initialize D-Array
	g_arrCreditList = new ArrayList( ByteCountToCells(UMG_CREDIT_NAME_SIZE) );
	g_arrCreditDBFieldName = new ArrayList( ByteCountToCells(UMG_CREDIT_FIELDNAME_SIZE) );

	// forward
	g_hGlobalForward_UMG_OnCreditRegistered = CreateGlobalForward("UMG_OnCreditRegistered", ET_Ignore, Param_String);
	g_hGlboalForward_UMG_OnTransactionPerform = CreateGlobalForward("UMG_OnTransactionPerform", ET_Event, Param_Cell, Param_String, Param_CellByRef);
	g_hGlobalForward_UMG_OnTransactionSucceeded = CreateGlobalForward("UMG_OnTransactionSucceeded", ET_Ignore, Param_Cell, Param_String, Param_Cell);
	g_hGlobalForward_UMG_OnTransactionFailed = CreateGlobalForward("UMG_OnTransactionFailed", ET_Ignore, Param_Cell, Param_String, Param_Cell, Param_Cell);

	// ConVar
	g_ConVarCreditTableName = CreateConVar("umg_credit_tablename", "UserInformation", "Set the tablename of Credits.");

}

// in fact, these're not necessary.
public void OnPluginEnd(){

	if(g_arrCreditList.Length == g_arrCreditDBFieldName.Length){

		for(int i = 0; i < g_arrCreditList.Length; i++){

			delete view_as<ArrayList>( g_arrUserCredit.Get(i) );
		}
	}
}

public void OnMapStart(){

	if(g_arrCreditList.Length == g_arrCreditDBFieldName.Length){

		for(int i = 0; i < g_arrCreditList.Length; i++){

			g_arrUserCredit.Push( new ArrayList(MaxClients+1) );
		}
	}

	else ThrowError("[umg_credit.sp] Array size must be same.");
}

// User-Manager Forward
public void UMG_OnDatabaseReady(Database dbIndex){

	char cQuery[512];
	char cTableName[MAX_TABLENAME_SIZE];
	char cFieldName[UMG_CREDIT_FIELDNAME_SIZE];

	Transaction txnHandle = new Transaction();

	for(int i = 0; i < g_arrCreditDBFieldName.Length; i++){

		g_ConVarCreditTableName.GetString(cTableName, sizeof(cTableName));
		g_arrCreditDBFieldName.GetString(i, cFieldName, sizeof(cFieldName));

		Format(cQuery, sizeof(cQuery), "ALTER TABLE %s ADD COLUMN %s int", cTableName, cFieldName);
		txnHandle.AddQuery(cQuery);

	}

	dbIndex.Execute(txnHandle, SQL_TxnOnSuccess_AlterCreditField, SQL_TxnOnFailure_AlterCreditField);

}

public void SQL_TxnOnSuccess_AlterCreditField(Database dbIndex, any iData, int numQueries, DBResultSet[] hResults, any[] anyQueryData){}
public void SQL_TxnOnFailure_AlterCreditField(Database dbIndex, any data, int numQueries, const char[] cError, int iIdxFail, any[] nothing){

	UMG_PrintAlert("[SQL_TxnOnFailure_AlterCreditField][Fail Index : %d] Error : %s", iIdxFail, cError);

}